package com.herprogramacion.kawaiicards;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    /*
    Declarar instancias globales
     */
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<Cartas> items = new ArrayList<>();
        int[] imagenes = {R.drawable.a, R.drawable.dos,R.drawable.tres,R.drawable.cuatro,R.drawable.cinco,R.drawable.seis,R.drawable.siete
                ,R.drawable.ocho,R.drawable.nueve,R.drawable.diez,R.drawable.jota,R.drawable.qu,R.drawable.ka};

        // Inicializar Cartas
        for (int i = 0; i < 6; i++) {
            items.add(new Cartas(imagenes[i],R.drawable.backrojo,Estat.BACK));
            items.add(new Cartas(imagenes[i],R.drawable.backrojo,Estat.BACK));
        }
        //Desordenar cartas
        Collections.shuffle(items);

        // Obtener el Recycler
        recycler = findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
     //   lManager = new LinearLayoutManager(this);
         lManager = new GridLayoutManager(this, 4);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new AnimeAdapter(items, this);
        recycler.setAdapter(adapter);


    }
}
