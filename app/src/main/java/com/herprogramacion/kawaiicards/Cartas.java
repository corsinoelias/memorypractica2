package com.herprogramacion.kawaiicards;


public class Cartas {
    private int front;
    private int back;
    private Estat estatus;

    public Cartas(int front,int back,Estat estat) {
        this.front = front;
        this.back=back;
        this.estatus=estat;
    }

    public void setEstatus(Estat estatus) {
        this.estatus = estatus;
    }

    public Estat getEstatus() {
        return estatus;
    }

    public int girar() {
        int retornarCarta=0;
        if (estatus.equals(Estat.BACK)){
            retornarCarta=back;
        }else if(estatus.equals(Estat.FRONT)){
            retornarCarta= front;
        }else if (estatus.equals(Estat.FIXED)){
            retornarCarta= front;
        }
        return retornarCarta;
    }


}
