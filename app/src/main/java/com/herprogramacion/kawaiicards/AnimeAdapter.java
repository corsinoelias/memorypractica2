package com.herprogramacion.kawaiicards;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Creado por Hermosa Programación
 */
public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder> {
    private List<Cartas> items;
    private Context context;
    private Cartas carta1=null;
    private Cartas carta2=null;
    public  class AnimeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imagen;
        public AnimeViewHolder(View v) {
            super(v);
            imagen =  v.findViewById(R.id.front);
            imagen.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
if (items.get(getPosition()).getEstatus()==Estat.FIXED ||items.get(getPosition()).getEstatus()==Estat.FRONT){
//Si la posició clicada está fija no tenemos que comprobar nada más.
}else{
    if (carta1==null){
        carta1=items.get(getPosition());
        items.get(getPosition()).setEstatus(Estat.FRONT);
        notifyDataSetChanged();
    }else{
        if (carta2==null){
            carta2=items.get(getPosition());
            items.get(getPosition()).setEstatus(Estat.FRONT);
            if (carta1.girar()==carta2.girar()){
                carta1.setEstatus(Estat.FIXED);
                carta2.setEstatus(Estat.FIXED);
                carta1=null;
                carta2=null;

            }else{
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run(){

                        carta1.setEstatus(Estat.BACK);
                        carta2.setEstatus(Estat.BACK);
                        carta1=null;
                        carta2=null;
                        notifyDataSetChanged();

                    }

                } , 4000);
            }



        }

    }
    notifyDataSetChanged();
}


        }
    }

    public AnimeAdapter(List<Cartas> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public AnimeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.anime_card, viewGroup, false);
        return new AnimeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AnimeViewHolder viewHolder, int i) {
        viewHolder.imagen.setImageResource(items.get(i).girar());
    }
}
